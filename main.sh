#!/bin/bash

#########################################################
# Before running this script, Arch Install needs to be running.
#
# After arch iso loads, and you are at command prompt, we need to get the dependencies to run the script.
#   Run... sudo pacman -Syy
#   Run... sudo pacman -S git
#   Run... git clone https://gitlab.com/ama.do/chiron.git
#   Configure Wifi if needed...
#   Set config variables, in the config.sh file
#   Run... cd chiron
#   Run... ./main.sh
#########################################################

# Get the config variables file
. config.sh

# Test internet connectivity
ping -c1 -q $TEST_HOST &>/dev/null
if [ ! $? -eq 0 ]
then
    echo "No Internet. Connect and try again."
    exit 1 # No Internet
fi

# Set time
timedatectl set-ntp true

# Show time
timedatectl status

# Set up partitions
sfdisk $BASE_DRIVE_LABEL <<EOF
 $SFDISK_CONFIG
EOF

# Make File Systems
mkfs.$UEFI_FS $UEFI_DRIVE
mkfs.$ROOT_FS $ROOT_DRIVE
mkfs.$HOME_FS $HOME_DRIVE

#BEGIN: mount the drives
mount $ROOT_DRIVE /mnt

mkdir /mnt/home
mount $HOME_DRIVE /mnt/home

mkdir /mnt/boot
mkdir /mnt/boot/EFI
mount $UEFI_DRIVE /mnt/boot/EFI 
#END: mount the drives

# Check the drives
#lsblk -l

# Install arch
pacstrap /mnt base linux linux-firmware --noconfirm --needed

# Generate FileSystem Table
genfstab -U /mnt >> /mnt/etc/fstab

# Copy the scripts to the installed OS for running
mkdir "/mnt/$TEMP_DIR_NAME"
cp * "/mnt/$TEMP_DIR_NAME"

# Change to root of installation
arch-chroot /mnt /bin/bash -e -x "./$TEMP_DIR_NAME/arch_chroot.sh" "/$TEMP_DIR_NAME"

# Remove temporary directory from installer script
rm -r "/mnt/$TEMP_DIR_NAME"

# Reboot without installer ISO in drive
if [ $SHUTDOWN_AFTER_INSTALL = 1 ]
then
    shutdown now
fi

