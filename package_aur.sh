#!/bin/bash

#########################################################
# Install additional packages that we need 
#########################################################

# Get util functions
. util.sh

DESKTOP_ENVIRONMENT=(
    #"polybar"
    #"vscodium-git"
)

yay_install_array "${DESKTOP_ENVIRONMENT[@]}"
