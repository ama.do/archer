#!/bin/bash

#########################################################
# Set the install script parameters here
# 
# Variables with ## !IMPORTANT! ## are more specific to a user/machine.
#   Be sure to change those.
#########################################################

# git repo host credentials MUST BE url encoded
GIT_HOST_USER="ama.do"
GIT_HOST_PWD="IMPORTANT_CHANGE"     ## !IMPORTANT! ##

# Declare the root pwd variable
ROOT_PWD="IMPORTANT_CHANGE"         ## !IMPORTANT! ##

# Create a user
ADD_USER="amado"                    ## !IMPORTANT! ##
ADD_USER_PWD="IMPORTANT_CHANGE"     ## !IMPORTANT! ##
ADD_USER_GROUPS="wheel,audio,video"

# Require password reset
PASSWORD_RESET=0    # true = 1, false anything else

# Require shutdown after install
SHUTDOWN_AFTER_INSTALL=0    # true = 1, false anything else

# Specify the drive
BASE_DRIVE_LABEL="/dev/sda"

#BEGIN: Partions
# UEFI
UEFI_PARTITION_SIZE="550M"
UEFI_FS="fat -F32"
UEFI_DRIVE="${BASE_DRIVE_LABEL}1"

# Root
ROOT_PARTITION_SIZE="25G"   ## !IMPORTANT! ##
ROOT_FS="ext4"
ROOT_DRIVE="${BASE_DRIVE_LABEL}2"

# Home
HOME_PARTITION_SIZE=""
HOME_FS="ext4"
HOME_DRIVE="${BASE_DRIVE_LABEL}3"

# Sfdisk Config = put it all together. 
# No reason to change this unless you want to change the order.
# Order matters because of *_DRIVE variables TODO: gotta be a smarter way
SFDISK_CONFIG="
label: gpt
, $UEFI_PARTITION_SIZE, U, * 
, $ROOT_PARTITION_SIZE, L, 
, $HOME_PARTITION_SIZE, L,
"
#End: Partions

# Region to set local time
REGION="America"

# City to set local time
CITY="New_York"

# Set computer hostname
HOSTNAME="something-new"    ## !IMPORTANT! ##

# Set Locale
LOCALE_LANG="en_US.UTF-8"
LOCALE_COLLATE="en_US.UTF-8"
LOCALE_TIME="en_US.UTF-8"
LOCALE_KEYMAP="us"

# Set required packages string
PACMAN_REQUIRED="sudo base-devel"

# Set bootloader packages
PACMAN_BOOT="grub efibootmgr dosfstools os-prober mtools"

# Set basic install packages
PACMAN_BASIC="networkmanager git nano go" # go is for yay

# Site to test internet connectivitiy with
TEST_HOST="google.com"

# Set arch-chroot script temp directory
TEMP_DIR_NAME="chiron"
