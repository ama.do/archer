#!/bin/bash

#########################################################
# Install additional packages that we need 
#########################################################

# Get util functions
. util.sh

DESKTOP_ENVIRONMENT_MIN=(
    "xorg"
    "xorg-xinit"
    #"nitrogen"
    "feh"
    "picom"
    "alacritty"
    "firefox"
    "lightdm"
    "lightdm-webkit2-greeter"
    "sxhkd"
    "bspwm"
    "rofi"
    "fuse3"     # for AppImages
    "libnotify" # app notifications
    "dunst"     # app notifications
    "fzf"       # for showkeys script
    "code"
)

pacman_install_array "${DESKTOP_ENVIRONMENT_MIN[@]}"
