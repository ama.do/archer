#!/bin/bash

#########################################################
# Util File to store reusable functions
#########################################################
# $1 is package name
pacman_install() {   
    pacman -S $1 --noconfirm --needed
}

# $1 is array of packages
pacman_install_array() {
    arr=("$@")
    pacman_install "${arr[*]// /|}"
}

# $1 is key
# $2 is value
# $3 is file
set_config() {
    # Comments tend to have a space after the #, and commented configs tend to not have the space.
    sed -i "s/^\(#\?$1*\s\?=\s*\).*/$1=$2/g" $3
}

# $1 user
# $2 command string
su_cmd_as_user() {
    su -c "$2" $1
}

# $1 is package name
yay_install() {   
    yay -S $1 --noconfirm --needed
}

# $1 is array of packages
yay_install_array() {
    arr=("$@")
    yay_install "${arr[*]// /|}"
}
