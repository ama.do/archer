#!/bin/bash

#########################################################
# Automate bringing down a system's configs
# 
# Based on https://www.atlassian.com/git/tutorials/dotfiles
##git #######################################################

# $1 = base path 
# $2 = directory
# $3 = work tree
# $4 = URL
# $5 = user

GIT_COMMAND="/usr/bin/git --git-dir=$1/$2 --work-tree=$3"

# clone the repo
runuser -l $5 -c "git clone --bare $4 \"$1/$2\""

# exlude this directory locally
echo "$2" >> "$1/$2/info/exclude"

# Get the files and overwrite if they exist on current install
runuser -l $5 -c "$GIT_COMMAND reset --hard"

# Set show untracked flag
$GIT_COMMAND config --local status.showUntrackedFiles no