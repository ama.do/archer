#!/bin/bash

#########################################################
# When we arch-chroot into the newly loaded install of arch, we need to call this script to finish off the install
#########################################################

# change working directory to the passed in directory
cd $1

# Get the config variables file
. config.sh

# Get util functions
. util.sh

# Set Local Time
ln -sf /usr/share/zoneinfo/${REGION}/${CITY} /etc/localtime

# Set Clock
hwclock --systohc

# Set Locale
localectl set-locale LANG="$LOCALE_LANG" LC_COLLATE="$LOCALE_COLLATE" LC_TIME="$LOCALE_TIME"
localectl set-keymap $LOCALE_KEYMAP

# Finalize Locale
locale-gen

# install "required" packages
pacman_install "$PACMAN_REQUIRED"

# Install Boot loader Packages
pacman_install "$PACMAN_BOOT"

# Install Boot loader
grub-install --target=x86_64-efi --bootloader-id=grub_uefi --recheck 

# Config grub
grub-mkconfig -o /boot/grub/grub.cfg

# Install basic packages
pacman_install "$PACMAN_BASIC"

# Congfigure systemd
systemctl enable NetworkManager

# Set Hostname
hostnamectl set-hostname $HOSTNAME

# Set Password for root
echo "root:$ROOT_PWD" | chpasswd 

# Add a new user
useradd -m $ADD_USER
echo "$ADD_USER:$ADD_USER_PWD" | chpasswd

# Add user to groups
usermod -aG "$ADD_USER_GROUPS" $ADD_USER

# undo allow sudo with no password for yay install
sed -i 's/# %wheel ALL=(ALL) NOPASSWD: ALL/%wheel ALL=(ALL) NOPASSWD: ALL/g' /etc/sudoers

#BEGIN: Install yay AUR helper
mkdir /usr/local/src/yay-git

# Change dir group to wheel so non-root user (but someone with "near root" expectations) can install this
chgrp -R wheel /usr/local/src

# grant write permissions so that we can build
chmod -R g+w /usr/local/src/yay-git

runuser -l $ADD_USER -c "git clone https://aur.archlinux.org/yay-git.git /usr/local/src/yay-git"

#TODO: there is a confirm message when yay installs.. Need to pass the return so that it skips it.
runuser -l $ADD_USER -c "cd /usr/local/src/yay-git; printf 'Y' | makepkg -i"
#END: Install yay AUR helper

# make script executable
chmod +x package_aur.sh

# Install aur packages
runuser -l $ADD_USER -c "cd $1; printf 'Y' | ./package_aur.sh"

# Install other packages:
. package.sh

# Edit config files for installed software #TODO: Will be moved to a git bare repo and this step will restore from the bare repo
. package_config.sh

# Bring down user's dotfiles and remaining configs from their git bare repo.
. git_bare.sh "/home/$ADD_USER" ".forge" "/home/$ADD_USER" "https://$GIT_HOST_USER:$GIT_HOST_PWD@gitlab.com/ama.do/forge.git" $ADD_USER

# Bring down system config from the git bare repo.
. git_bare.sh "/usr/local/src" "anvil" "/etc" "https://$GIT_HOST_USER:$GIT_HOST_PWD@gitlab.com/ama.do/anvil.git" "root"

# undo allow sudo with no password for yay install
sed -i 's/%wheel ALL=(ALL) NOPASSWD: ALL/# %wheel ALL=(ALL) NOPASSWD: ALL/g' /etc/sudoers

# Change wheel group in sudoers file
sed -i 's/# %wheel ALL=(ALL) ALL/%wheel ALL=(ALL) ALL/g' /etc/sudoers

# Force Password Expiration of passwords in case a user forgot to change the password from config file
if [ $PASSWORD_RESET = 1 ]
then
    passwd root -e
    passwd $ADD_USER -e
fi

# Exit to main script
exit
